/**
 * @file
 * Entry point for ReactJS and custom module builds.
 *
 */
var React = require('react');
var ReactDOM = require('react-dom');
